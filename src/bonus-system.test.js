import { calculateBonuses } from "./bonus-system.js";

describe('Calculator tests', () => {

    console.log("Tests started");

    test('Bonus for program "Standard" and amount 1', (done) => {
        expect(calculateBonuses('Standard', 1)).toEqual(0.05 * 1);
        done();
    })

    test('Bonus for program "Premium" and amount 100', (done) => {
        expect(calculateBonuses('Premium', 100)).toEqual(0.1 * 1);
        done();
    })

    test('Bonus for program "Diamond" and amount 1000', (done) => {
        expect(calculateBonuses('Diamond', 1000)).toEqual(0.2 * 1);
        done();
    })

    test('Bonus for program "Standard" and amount 10000', (done) => {
        expect(calculateBonuses('Standard', 10000)).toEqual(0.05 * 1.5);
        done();
    })

    test('Bonus for program "Premium" and amount 25000', (done) => {
        expect(calculateBonuses('Premium', 25000)).toEqual(0.1 * 1.5);
        done();
    })

    test('Bonus for program "Diamond" and amount 45000', (done) => {
        expect(calculateBonuses('Diamond', 45000)).toEqual(0.2 * 1.5);
        done();
    })

    test('Bonus for program "Standard" and amount 50000', (done) => {
        expect(calculateBonuses('Standard', 50000)).toEqual(0.05 * 2);
        done();
    })

    test('Bonus for program "Premium" and amount 75000', (done) => {
        expect(calculateBonuses('Premium', 75000)).toEqual(0.1 * 2);
        done();
    })

    test('Bonus for program "Diamond" and amount 95000', (done) => {
        expect(calculateBonuses('Diamond', 95000)).toEqual(0.2 * 2);
        done();
    })

    test('Bonus for program "Standard" and amount 100000', (done) => {
        expect(calculateBonuses('Standard', 100000)).toEqual(0.05 * 2.5);
        done();
    })

    test('Bonus for program "Premium" and amount 150000', (done) => {
        expect(calculateBonuses('Premium', 150000)).toEqual(0.1 * 2.5);
        done();
    })

    test('Bonus for program "Diamond" and amount 200000', (done) => {
        expect(calculateBonuses('Diamond', 200000)).toEqual(0.2 * 2.5);
        done();
    })

    test('Bonus for program "Invalid" and amount 1', (done) => {
        expect(calculateBonuses('Invalid', 1)).toEqual(0);
        done()
    })

    test('Bonus for program "Invalid" and amount 10000', (done) => {
        expect(calculateBonuses('Invalid', 10000)).toEqual(0);
        done()
    })

    test('Bonus for program "Invalid" and amount 50000', (done) => {
        expect(calculateBonuses('Invalid', 50000)).toEqual(0);
        done()
    })

    test('Bonus for program "Invalid" and amount 100000', (done) => {
        expect(calculateBonuses('Invalid', 100000)).toEqual(0);
        done()
    })

    console.log('Tests Finished');

});